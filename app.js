const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  }
];

const container = document.querySelector('.root');
const list = document.createElement('ul');

books.forEach(book => {
  const LI = document.createElement('li');
  const { author, name, price } = book;

  try {
    if (!author) {
      throw new Error("Author was not specified.");
    } else if (!name) {
      throw new Error("Name was not specified.");
    } else if (!price) {
      throw new Error("Price was not specified.");
    }

    LI.textContent = `${author} "${name}". Price: ${price}`;
    list.append(LI);
  } catch (e) {
    console.error(`Oops, book info incomplete. ${e.message}`);
  }
});

container.append(list);